package com.example.todo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.todo.databinding.ItemViewBinding;
import com.example.todo.model.GetTasksData;
import com.example.todo.model.Task;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.MyViewHolder> {
    private ArrayList<Task> tasks;

    public TodoAdapter(){
        this.tasks = new ArrayList<>();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemViewBinding binding;//Name of the item_view.xml in camel case + "Binding"

        public MyViewHolder(ItemViewBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }

    @Override
    public TodoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new MyViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Get the data model based on position
        Task task = tasks.get(position);

        holder.binding.description.setText(task.getDescription());
        holder.binding.createdAt.setText(task.getCreatedAt());
    }


    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public int getId(int position){

        return this.tasks.get(position).getId();
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    public Task getAt(int position){
        Task task;
        task = this.tasks.get(position);
        return task;
    }

    public void add(Task task) {
        this.tasks.add(task);
        notifyItemInserted(tasks.size() - 1);
        notifyItemRangeChanged(0, tasks.size() - 1);
    }

    public void modifyAt(Task task, int position) {
        this.tasks.set(position, task);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        this.tasks.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, tasks.size() - 1);
    }
}
