package com.example.todo.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.R;
import com.example.todo.databinding.ActivityAddBinding;
import com.example.todo.databinding.ActivityMainBinding;
import com.example.todo.model.AddResponse;
import com.example.todo.model.Task;
import com.example.todo.network.ApiTokenRestClient;
import com.example.todo.util.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements View.OnClickListener, Callback<AddResponse> {
    public static final int OK = 1;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;

    private ActivityAddBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityAddBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.accept.setOnClickListener(this);
        binding.cancel.setOnClickListener(this);

        preferences = new SharedPreferencesManager(this);
    }

    @Override
    public void onClick(View v) {

        String description;
        Task task;

        if (v == binding.accept) {
            hideSoftKeyboard();
            description = binding.editText.getText().toString();
            if (description.isEmpty())
                Toast.makeText(this, "Please, fill the description", Toast.LENGTH_SHORT).show();
            else {
                task = new Task(description);
                connection(task);
            }
        } else if (v == binding.cancel) {
            finish();
        }
    }

    private void connection(Task task) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Site> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        Call<AddResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).createTask(task);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddResponse> call, Response<AddResponse> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            AddResponse addResponse = response.body();
            if (addResponse.getSuccess()) {
                Intent i = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("id", addResponse.getData().getId());
                bundle.putString("description", addResponse.getData().getDescription());
                bundle.putString("createdAt", addResponse.getData().getCreatedAt());
                i.putExtras(bundle);
                setResult(OK, i);
                finish();
                showMessage("Task created ok");
            } else {
                String message = "Error creating the task";
                if (!addResponse.getMessage().isEmpty()) {
                    message += ": " + addResponse.getMessage();
                }
                showMessage(message);

            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: ");
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<AddResponse> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

