package com.example.todo.network;

import com.example.todo.model.DeleteResponse;
import com.example.todo.model.Email;
import com.example.todo.model.AddResponse;
import com.example.todo.model.EmailResponse;
import com.example.todo.model.LoginResponse;
import com.example.todo.model.LogoutResponse;
import com.example.todo.model.RegisterResponse;
import com.example.todo.model.Task;
import com.example.todo.model.GetTasksResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiTokenService {

    @POST("api/logout")
    Call<LogoutResponse> logout(
            //@Header("Authorization") String token
    );

    @GET("api/tasks")
    Call<GetTasksResponse> getTasks(
            //@Header("Authorization") String token
    );

    @POST("api/tasks")
    Call<AddResponse> createTask(
            //@Header("Authorization") String token,
            @Body Task task);

    @PUT("api/tasks/{id}")
    Call<AddResponse> updateTask(
            //@Header("Authorization") String token,
            @Body Task task,
            @Path("id") int id);

    @DELETE("api/tasks/{id}")
    Call<DeleteResponse> deleteTask(
            //@Header("Authorization") String token,
            @Path("id") int id);

    @POST("api/email")
    Call<EmailResponse> sendEmail(@Body Email email);
}

