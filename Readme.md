App en Android que se comunica con el API Rest en Laravel

Dominio usado: [todo.alumnoportada.com.es](https://todo.alumnoportada.com.es)

Permite registrarse y hacer login.

Una vez hecho login se puede:

    - Ver las tareas del usuario

    - Añadir una nueva tarea

    - Modificar una tarea

    - Eliminar una tarea

    - Salir